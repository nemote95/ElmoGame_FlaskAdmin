from application.config import DefaultConfig
from flask import Flask
from extension import db, admin


def configure_app(app, configuration=DefaultConfig):
    app.config.from_object(configuration)
    app.config.from_pyfile('environ.py', silent=True)


def configure_admin(app):
    from flask_admin.contrib.mongoengine import ModelView
    import adminView.gifts,adminView.users,adminView.soccerPlayers
    from models import ad_tokens, gifts, users, banners, soccerPlayers, powerups, configuration, formation,news
    admin.init_app(app)
    admin.add_view(ModelView(configuration.Configurations))
    admin.add_view(adminView.users.UsersView(users.Users))
    admin.add_view(adminView.soccerPlayers.SoccerPlayersView(soccerPlayers.SoccerPlayers))
    admin.add_view(ModelView(powerups.Powerups))
    admin.add_view(ModelView(banners.Banners))
    admin.add_view(ModelView(formation.Formations))
    admin.add_view(ModelView(ad_tokens.Ad_tokens))
    admin.add_view(adminView.gifts.GiftsView(gifts.Gifts))
    admin.add_view(ModelView(news.News))


def create_app():
    app = Flask(__name__)
    configure_app(app)
    configure_admin(app)
    db.init_app(app)
    return app
