import os

basedir = os.path.abspath(os.path.dirname(__file__))


class DefaultConfig(object):
    DEBUG = True
    DEPLOYMENT = False

    SECRET_KEY = os.environ.get('SECRET_KEY') or 'developer'
    SECURITY_PASSWORD_SALT = os.environ.get('SECURITY_PASSWORD_SALT') or 'developers_ps'

    TEMPLATE = ''

    MONGODB_SETTINGS = {'DB': 'FootballTest'}

    #MONGODB_SETTINGS = {
    #'db': 'FootballTest',
    #'host': '5.61.24.119',
    #'port': 978
    #	}

    # Blueprint need to be installed entered here
    INSTALLED_CONTROLLERS = (

    )


class DevelopmentConfig(DefaultConfig):
    DEBUG = True


config = {
    'development': DevelopmentConfig,
    'default': DevelopmentConfig
}
