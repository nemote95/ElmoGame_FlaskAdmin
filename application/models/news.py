from application.extension import db
import datetime


class EmbeddedNews(db.EmbeddedDocument):
    Title = db.StringField(required=True)
    Text = db.StringField(required=True)
    Date = db.DateTimeField(required=True, default=datetime.datetime.now)

    def __unicode__(self):
        return self.Title


class News(db.Document):
    News = db.EmbeddedDocumentListField(EmbeddedNews)