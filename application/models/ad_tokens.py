from application.extension import db
import datetime


class Ad_tokens(db.Document):
    Type = db.StringField(required=True)
    Watched = db.BooleanField(required=True)
    Claimed = db.BooleanField(required=True)
    Gen = db.DateTimeField(required=True, default=datetime.datetime.now)
