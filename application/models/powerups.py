from application.extension import db


class Powerups(db.Document):
    Price = db.IntField(required=True)
    Scale = db.IntField(required=True)
    Discount = db.IntField(required=True)
    CWE = db.IntField(required=True)
    Rating = db.IntField(required=True)
    Type = db.IntField(required=True)
    Price = db.IntField(required=True)
    Duration = db.IntField(required=True)
    Start_Speed = db.IntField(required=True)
    Drag = db.IntField(required=True)
    Mass = db.IntField(required=True)
    Stamina = db.IntField(required=True)
    Name = db.StringField(required=True)
