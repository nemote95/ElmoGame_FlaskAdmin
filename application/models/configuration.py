from application.extension import db


class GemPacks(db.EmbeddedDocument):
    gem_pack1 = db.ListField(db.IntField(), required=True, name="1")
    gem_pack2 = db.ListField(db.IntField(), required=True, name="2")
    gem_pack3 = db.ListField(db.IntField(), required=True, name="3")
    gem_pack4 = db.ListField(db.IntField(), required=True, name="4")
    gem_pack5 = db.ListField(db.IntField(), required=True, name="5")
    gem_pack6 = db.ListField(db.IntField(), required=True, name="6")


class GoldEPacks(db.EmbeddedDocument):
    golde_pack1 = db.ListField(db.IntField(), required=True, name="1")
    golde_pack2 = db.ListField(db.IntField(), required=True, name="2")
    golde_pack3 = db.ListField(db.IntField(), required=True, name="3")
    golde_pack4 = db.ListField(db.IntField(), required=True, name="4")
    golde_pack5 = db.ListField(db.IntField(), required=True, name="5")
    golde_pack6 = db.ListField(db.IntField(), required=True, name="6")


class RoomRewards(db.EmbeddedDocument):
    room_reward1 = db.IntField(required=True, name="1")
    room_reward2 = db.IntField(required=True, name="2")
    room_reward3 = db.IntField(required=True, name="3")
    room_reward4 = db.IntField(required=True, name="4")
    room_reward5 = db.IntField(required=True, name="5")


class AdPacks(db.EmbeddedDocument):
    t1 = db.IntField(required=True)
    t2 = db.IntField(required=True)
    t3 = db.IntField(required=True)
    v = db.IntField(required=True)
    t4 = db.IntField(required=True)
    t5 = db.IntField(required=True)
    t6 = db.IntField(required=True)


class GemEPacks(db.EmbeddedDocument):
    geme_pack1 = db.ListField(db.IntField(), required=True, name="1")
    geme_pack2 = db.ListField(db.IntField(), required=True, name="2")
    geme_pack3 = db.ListField(db.IntField(), required=True, name="3")
    geme_pack4 = db.ListField(db.IntField(), required=True, name="4")
    geme_pack5 = db.ListField(db.IntField(), required=True, name="5")
    geme_pack6 = db.ListField(db.IntField(), required=True, name="6")


class Rooms(db.EmbeddedDocument):
    room1 = db.IntField(required=True, name="1")
    room2 = db.IntField(required=True, name="2")
    room3 = db.IntField(required=True, name="3")
    room4 = db.IntField(required=True, name="4")
    room5 = db.IntField(required=True, name="5")


class GGPacks(db.EmbeddedDocument):
    gg_pack1 = db.ListField(db.IntField(), required=True, name="1")
    gg_pack2 = db.ListField(db.IntField(), required=True, name="2")
    gg_pack3 = db.ListField(db.IntField(), required=True, name="3")
    gg_pack4 = db.ListField(db.IntField(), required=True, name="4")
    gg_pack5 = db.ListField(db.IntField(), required=True, name="5")
    gg_pack6 = db.ListField(db.IntField(), required=True, name="6")


class Configurations(db.Document):
    Search_Engine = db.StringField(required=True)
    Gem_Packs = db.EmbeddedDocumentField(GemPacks)
    GoldE_Packs = db.EmbeddedDocumentField(GoldEPacks)
    Room_Rewards = db.EmbeddedDocumentField(RoomRewards)
    Ad_Packs = db.EmbeddedDocumentField(AdPacks)
    GemE_Packs = db.EmbeddedDocumentField(GemEPacks)
    Server_Status = db.StringField(required=True)
    FreeEnergy = db.IntField(required=True)
    Last_Guest = db.IntField(required=True)
    Rooms = db.EmbeddedDocumentField(Rooms)
    GG_Packs = db.EmbeddedDocumentField(GGPacks)
    Player_Version = db.StringField(required=True)
    Powerup_Version = db.StringField(required=True)
