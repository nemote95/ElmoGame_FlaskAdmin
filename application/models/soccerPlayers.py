from application.extension import db
#from powerups import Powerups


class SoccerPlayers(db.Document):
    Price = db.IntField(required=True)
    Speed = db.IntField(required=True)
    Active_Power = db.IntField(required=True)
    Drag = db.IntField(required=True)
    Rating = db.IntField(required=True)
    Size = db.IntField(required=True)
    Image = db.IntField(required=True)
    Scale = db.IntField(required=True)
    Position = db.IntField(required=True)
    AntiInjury = db.IntField(required=True)
    Health = db.IntField(required=True)
    CWE = db.IntField(required=True)
    Stamina = db.IntField(required=True)
    Power = db.IntField(required=True)
    Name = db.StringField(required=True)
    Powerups = db.DynamicField()
    Start_Speed = db.IntField(required=True)
    Nickname = db.StringField(required=True)
    Mass = db.IntField(required=True)
    Injury = db.IntField(required=True)

    meta = {'collection': "soccerPlayers"}

    def __unicode__(self):
        return self.Name
