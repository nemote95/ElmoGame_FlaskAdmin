from application.extension import db
from users import Users


class Gifts(db.Document):
    gems = db.IntField(required=True)
    user = db.ReferenceField(Users, required=True)
