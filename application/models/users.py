from application.extension import db


class Stats(db.DynamicDocument):
    Win = db.IntField(required=True, default=0)
    Lost = db.IntField(required=True, default=0)


class Users(db.Document):
    Active = db.ListField(db.IntField())
    Active_Banner = db.IntField(required=True)
    Active_Formation = db.IntField(required=True)
    Energy = db.IntField(required=True)
    Exp = db.IntField(required=True)
    Friends_Count = db.IntField(required=True)
    Gem = db.IntField(required=True)
    Gold = db.IntField(required=True)
    Image = db.IntField(required=True)
    Injury_Times = db.ListField(db.IntField())
    Is_Guest = db.BooleanField(required=True)
    Last_Free_Reward = db.DateTimeField(required=True)
    Level = db.IntField(required=True)
    Nickname = db.StringField(required=True)
    Online = db.BooleanField(required=True)
    Orientation = db.IntField(required=True)
    Owned_Banners = db.ListField(db.IntField())
    Owned_Formations = db.ListField(db.IntField())
    Password = db.StringField(required=True)
    Pending = db.ListField(db.IntField())
    Pending_Count = db.IntField(required=True)
    Reward_Multiplier = db.IntField(required=True)
    Roster = db.DynamicField()
    Friends = db.DynamicField()
    SessionId = db.ObjectIdField(required=True)
    Session_Expiration = db.DateTimeField(required=True)
    Stats = db.DictField(field=db.IntField())
    Username = db.StringField(required=True)

    def __unicode__(self):
        return self.Username
