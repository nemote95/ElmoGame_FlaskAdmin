from flask_admin.contrib.mongoengine import ModelView


class UsersView(ModelView):
    form_excluded_columns = ['Friends', 'Roster', 'Pending']
    column_exclude_list = ['Friends', 'Roster', 'Pending', 'Active','Owned_Banners']

