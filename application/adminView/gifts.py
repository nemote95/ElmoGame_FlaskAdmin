from flask_admin.contrib.mongoengine import ModelView


class GiftsView(ModelView):
    form_ajax_refs = {
        'user': {
            'fields': ['Username']
        }
    }
