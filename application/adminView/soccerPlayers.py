from flask_admin.contrib.mongoengine import ModelView


class SoccerPlayersView(ModelView):
    form_excluded_columns = ['Powerups']
    column_exclude_list = ['Powerups']

