from flask.ext.script import Manager, Shell
from application import create_app

app = create_app()
manager = Manager(app)


def make_shell_context():
    return dict(app=app)


manager.add_command("shell", Shell(make_context=make_shell_context))


@manager.command
def run():
    app.run(host='0.0.0.0', port=8080, debug=True)


if __name__ == '__main__':
    manager.run()
