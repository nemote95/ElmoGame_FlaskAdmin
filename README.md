
### Requirements
python 2.x

### How to run?
1. Go to project path
2. Create a virtual environment: `virtualenv venv`
alternative: if virtualenv haven't been installed use `sudo apt-get install virtual` to install it.
3. Go to created environment by `source venv/bin/activate`
4. Install project dependencies by `pip install -r requirments`
5. edit `MONGODB_SETTINGS`in `conifig.py`
6. Run project `python manage.py run`

